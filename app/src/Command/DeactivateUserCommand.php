<?php declare(strict_types=1);

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeactivateUserCommand extends Command
{
    protected static $defaultName = 'app:deactivate-user';
    protected EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure(): void
    {
        $this->setHelp('This command deactivates user with 30 days without login');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $count = $this->em->getRepository(User::class)->markInactive();
        $output->writeln($count);

        return Command::SUCCESS;
    }
}