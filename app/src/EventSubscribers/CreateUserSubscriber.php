<?php declare(strict_types=1);

namespace App\EventSubscribers;

use App\Entity\User;
use App\UseCase\SendCredencialsEmailUseCase;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\UseCase\CreateUserUseCase;

class CreateUserSubscriber implements EventSubscriberInterface
{
    private CreateUserUseCase $useCase;
    private SendCredencialsEmailUseCase $emailUseCase;

    public function __construct(CreateUserUseCase $useCase, SendCredencialsEmailUseCase $emailUseCase)
    {
        $this->useCase = $useCase;
        $this->emailUseCase = $emailUseCase;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            BeforeEntityPersistedEvent::class => ['createUser']
        ];
    }

    public function createUser(BeforeEntityPersistedEvent $event): void
    {
        $user = $event->getEntityInstance();
        if ($user instanceof User) {
//            $this->emailUseCase->sendEmail($user);
            $this->useCase->createUser($user);
        }
    }
}