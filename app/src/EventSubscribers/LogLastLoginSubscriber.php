<?php declare(strict_types=1);

namespace App\EventSubscribers;

use App\Entity\User;
use App\UseCase\UserLoginUserCase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\Event\LoginSuccessEvent;

class LogLastLoginSubscriber implements EventSubscriberInterface
{

    private UserLoginUserCase $useCase;

    public function __construct(UserLoginUserCase $useCase)
    {
        $this->useCase = $useCase;
    }

    public static function getSubscribedEvents()
    {
        return [
            LoginSuccessEvent::class => ['userLoggedIn']
        ];
    }

    public function userLoggedIn(LoginSuccessEvent $event)
    {
        $user = $event->getUser();
        if ($user instanceof User) {
            $this->useCase->logLoginTime($user);
        }
    }
}