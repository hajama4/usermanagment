<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

     /**
      * @return User[] Returns an array of User objects
      */
    public function markInactive()
    {
        return $this->createQueryBuilder('u')
            ->update()
            ->set('u.active', '0')
            ->andWhere('u.lastLoginAt < :days')
            ->andWhere('u.active = :active')
            ->setParameter('days', (new \DateTime())->modify('-30 days'))
            ->setParameter('active', 1)
            ->getQuery()
            ->execute();
        ;
    }
}
