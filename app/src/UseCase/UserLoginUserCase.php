<?php declare(strict_types=1);

namespace App\UseCase;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class UserLoginUserCase
{

    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    public function logLoginTime(User $user)
    {
        $user->setLastLoginAt(new \DateTime());
        $user->setActive(true);

        $this->em->persist($user);
        $this->em->flush();
    }

}