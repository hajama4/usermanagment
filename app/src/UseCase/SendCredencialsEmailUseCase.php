<?php declare(strict_types=1);

namespace App\UseCase;

use App\Entity\User;
//use App\Security\EmailVerifier;

class SendCredencialsEmailUseCase
{
    private EmailVerifier $emailVerifier;

//    public function __construct(EmailVerifier $emailVerifier)
//    {
//        $this->emailVerifier = $emailVerifier;
//    }

    public function sendEmail(User $user)
    {
        $this->emailVerifier->sendEmailConfirmation('app_verify_email', $user,
            (new TemplatedEmail())
                ->from(new Address('asasis1@gmail.com', 'Tessst'))
                ->to($user->getEmail())
                ->subject('Please Confirm your Email')
                ->htmlTemplate('registration/confirmation_email.html.twig')
        );
    }

}